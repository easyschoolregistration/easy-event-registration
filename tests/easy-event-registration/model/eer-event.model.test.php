<?php
include_once EER_PLUGIN_PATH . '/tests/eer-base-test.php';

class EER_Event_Model_Test extends PHPUnit_Framework_TestCase2 {

	private $worker_event;

	private $base_test;


	public function __construct() {
		parent::__construct();
		$this->worker_event = new EER_Worker_Event();
		$this->base_test    = new EER_Base_Test();
	}


	public function setUp() {
		$this->base_test->delete_all_data();
		$this->base_test->setUp();
	}


	public function test_is_event_active() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$event = $this->base_test->eer_get_event_by_title('Test Event');
		$this->assertEquals(true, EER()->event->is_event_sale_active($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("-10 days")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("-5 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(false, EER()->event->is_event_sale_active($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("+5 days")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+10 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(false, EER()->event->is_event_sale_active($event->id));
	}


	public function test_is_event_closed() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$event = $this->base_test->eer_get_event_by_title('Test Event');
		$this->assertEquals(false, EER()->event->is_event_sale_closed($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("+1 day")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+3 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(false, EER()->event->is_event_sale_closed($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("-5 days")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("-3 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(true, EER()->event->is_event_sale_closed($event->id));
	}


	public function test_is_event_not_opened_yet() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$event = $this->base_test->eer_get_event_by_title('Test Event');
		$this->assertEquals(false, EER()->event->is_event_sale_not_opened_yet($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("-5 days")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("-3 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(false, EER()->event->is_event_sale_not_opened_yet($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s', strtotime("+3 days")),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+5 days")),
			'event_settings' => ''
		]);

		$this->assertEquals(true, EER()->event->is_event_sale_not_opened_yet($event->id));
	}


	public function test_get_event_title() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$event = $this->base_test->eer_get_event_by_title('Test Event');
		$this->assertEquals('Test Event', EER()->event->get_event_title($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event 2',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$this->assertEquals('Test Event 2', EER()->event->get_event_title($event->id));

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event 3',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$this->assertEquals('Test Event 3', EER()->event->get_event_title($event->id));
	}

}
