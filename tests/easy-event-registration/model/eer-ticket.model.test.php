<?php
include_once EER_PLUGIN_PATH . '/tests/eer-base-test.php';

class EER_Ticket_Model_Test extends PHPUnit_Framework_TestCase2 {

	private $worker_event;

	private $worker_ticket;

	private $base_test;


	public function __construct() {
		parent::__construct();
		$this->worker_event  = new EER_Worker_Event();
		$this->worker_ticket = new EER_Worker_Ticket();
		$this->base_test     = new EER_Base_Test();
	}


	public function setUp() {
		$this->base_test->delete_all_data();
		$this->base_test->setUp();
	}


	public function test_is_solo() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->assertEquals(false, (boolean) EER()->ticket->eer_is_solo($ticket->id));

		$this->worker_ticket->process_ticket([
			'ticket_id'       => $ticket->id,
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => true,
			'ticket_settings' => ''
		]);

		$this->assertEquals(true, (boolean) EER()->ticket->eer_is_solo($ticket->id));
	}


	public function test_get_ticket_data() {
		$this->assertEquals(null, EER()->ticket->get_ticket_data(null));
		$this->assertEquals(null, EER()->ticket->get_ticket_data(0));
	}


	public function test_check_ticket_exists() {
		$this->assertEquals(null, (boolean) EER()->ticket->eer_check_ticket_exists(0));

		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->assertEquals(true, (boolean) EER()->ticket->eer_check_ticket_exists($ticket->id));
	}


	public function test_is_ticket_buy_enabled() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->assertEquals(false, (boolean) EER()->ticket->is_ticket_buy_enabled($ticket->id));


		$this->worker_ticket->process_ticket([
			'ticket_id'       => $ticket->id,
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_leaders'     => 10,
			'max_followers'   => 10,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$this->assertEquals(true, (boolean) EER()->ticket->is_ticket_buy_enabled($ticket->id));

		$this->worker_ticket->process_ticket([
			'ticket_id'       => $ticket->id,
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => true,
			'ticket_settings' => ''
		]);

		$this->assertEquals(false, (boolean) EER()->ticket->is_ticket_buy_enabled($ticket->id));

		$this->worker_ticket->process_ticket([
			'ticket_id'       => $ticket->id,
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'        => 30,
			'is_solo'         => true,
			'ticket_settings' => ''
		]);

		$this->assertEquals(true, (boolean) EER()->ticket->is_ticket_buy_enabled($ticket->id));
	}

}
