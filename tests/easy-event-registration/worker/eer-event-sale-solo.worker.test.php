<?php
include_once EER_PLUGIN_PATH . '/tests/eer-base-test.php';

class EER_Event_Sale_Solo_Worker_Test extends PHPUnit_Framework_TestCase2 {

	private $worker_event;

	private $worker_event_sale;

	private $worker_ticket;

	private $base_test;


	public function __construct() {
		parent::__construct();
		$this->worker_event      = new EER_Worker_Event();
		$this->worker_ticket     = new EER_Worker_Ticket();
		$this->worker_event_sale = new EER_Worker_Event_Sale();
		$this->base_test         = new EER_Base_Test();
	}


	public function setUp() {
		$this->base_test->delete_all_data();
		$this->base_test->setUp();
	}


	public function test_solo_registration_errors() {
		$this->assertEquals(0, count(EER()->ticket->load_tickets()));

		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'     => 30,
			'is_solo'         => true,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => [],
			'tickets'   => [],
		], true);

		$this->assertArrayHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		do_action('eer_process_event', [
			'event_id'       => $event->id,
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => [
				'phone_required'   => 1,
				'country_required' => 1
			]
		]);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => [],
			'tickets'   => [],
		], true);

		$this->assertArrayHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name' => 'Karl'
			],
			'tickets'   => [],
		], true);

		$this->assertArrayNotHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl'
			],
			'tickets'   => [],
		], true);

		$this->assertArrayNotHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
			],
			'tickets'   => [],
		], true);

		$this->assertArrayNotHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890'
			],
			'tickets'   => [],
		], true);

		$this->assertArrayNotHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [],
		], true);

		$this->assertArrayNotHasKey('user_info.name', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.surname', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.email', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.phone', $errors['errors']->errors);
		$this->assertArrayNotHasKey('user_info.country', $errors['errors']->errors);
		$this->assertArrayHasKey('tickets.all.empty', $errors['errors']->errors);

		$errors = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertEquals(false, isset($errors['errors']));
	}


	public function test_solo_registration_one_ticket() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => [
				'phone_required'   => 1,
				'country_required' => 1
			]
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->assertEquals(0, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(0, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(0, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'     => 30,
			'is_solo'         => true,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertEquals(1, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(1, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(1, count(EER()->payment->eer_get_payments_by_event($event->id)));
	}


	public function test_solo_registration_manual() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => [
				'phone_required'   => 1,
				'country_required' => 1
			]
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->assertEquals(0, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(0, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(0, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'     => 30,
			'is_solo'         => true,
			'pairing_mode'    => EER_Enum_Pairing_Mode::MANUAL,
			'ticket_settings' => ''
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 3
				]
			],
		], true);

		//$user = $this->base_test->eer_get_user_id_by_email('karl@easyschoolregistration.com');

		$this->assertEquals(3, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(1, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(0, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals(0, $summary->registered_leaders);
		$this->assertEquals(0, $summary->registered_followers);
		$this->assertEquals(0, $summary->registered_tickets);
		$this->assertEquals(0, $summary->waiting_leaders);
		$this->assertEquals(0, $summary->waiting_followers);
		$this->assertEquals(3, $summary->waiting_tickets);

		$this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertEquals(4, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(2, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(0, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals(0, $summary->registered_leaders);
		$this->assertEquals(0, $summary->registered_followers);
		$this->assertEquals(0, $summary->registered_tickets);
		$this->assertEquals(0, $summary->waiting_leaders);
		$this->assertEquals(0, $summary->waiting_followers);
		$this->assertEquals(4, $summary->waiting_tickets);

		$sold_tickets = EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id);

		$registered_leaders = 0;
		$registered_followers = 0;
		$registered_tickets = 0;
		$waiting_leaders = 0;
		$waiting_followers = 0;
		$waiting_tickets = 4;
		foreach ($sold_tickets as $id => $sold_ticket) {
			$this->base_test->worker_ajax->confirm_sold_ticket($id);
			$summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

			$this->assertEquals($registered_leaders, $summary->registered_leaders);
			$this->assertEquals($registered_followers, $summary->registered_followers);
			$this->assertEquals(++$registered_tickets, $summary->registered_tickets);
			$this->assertEquals($waiting_leaders, $summary->waiting_leaders);
			$this->assertEquals($waiting_followers, $summary->waiting_followers);
			$this->assertEquals(--$waiting_tickets, $summary->waiting_tickets);
		}

		$this->assertEquals(4, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(2, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(2, count(EER()->payment->eer_get_payments_by_event($event->id)));
	}


	public function test_solo_registration_confirm_all() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => []
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->assertEquals(0, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(0, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(0, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'     => 30,
			'is_solo'         => true,
			'pairing_mode'    => EER_Enum_Pairing_Mode::CONFIRM_ALL,
			'ticket_settings' => []
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 2
				]
			],
		], true);

		$this->assertEquals(2, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(1, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(1, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals(0, $summary->registered_leaders);
		$this->assertEquals(0, $summary->registered_followers);
		$this->assertEquals(2, $summary->registered_tickets);
		$this->assertEquals(0, $summary->waiting_leaders);
		$this->assertEquals(0, $summary->waiting_followers);
		$this->assertEquals(0, $summary->waiting_tickets);

		$this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com',
				'phone'   => '1234567890',
				'country' => 'CZ'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertEquals(3, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(2, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(2, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals(0, $summary->registered_leaders);
		$this->assertEquals(0, $summary->registered_followers);
		$this->assertEquals(3, $summary->registered_tickets);
		$this->assertEquals(0, $summary->waiting_leaders);
		$this->assertEquals(0, $summary->waiting_followers);
		$this->assertEquals(0, $summary->waiting_tickets);
	}


	public function test_solo_registration_full() {
		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => []
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'max_tickets'     => 1,
			'is_solo'         => true,
			'ticket_settings' => []
		]);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$result = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertEquals(1, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(1, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(1, count(EER()->payment->eer_get_payments_by_event($event->id)));

		$result = $this->worker_event_sale->process_registration((object) [
			'event_id'  => $event->id,
			'user_info' => (object) [
				'name'    => 'Karl',
				'surname' => 'Karl',
				'email'   => 'karl@easyschoolregistration.com'
			],
			'tickets'   => [
				$ticket->id => (object) [
					'number_of_tickets' => 1
				]
			],
		], true);

		$this->assertArrayHasKey('tickets.' . $ticket->id . '.full', $result['errors']->errors);

		$this->assertEquals(1, count(EER()->sold_ticket->eer_get_sold_tickets_by_event($event->id)));
		$this->assertEquals(1, count(EER()->order->eer_get_orders_by_event($event->id)));
		$this->assertEquals(1, count(EER()->payment->eer_get_payments_by_event($event->id)));
	}

}
