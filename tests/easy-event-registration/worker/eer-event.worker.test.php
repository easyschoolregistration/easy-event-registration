<?php
include_once EER_PLUGIN_PATH . '/tests/eer-base-test.php';

class EER_Event_Worker_Test extends PHPUnit_Framework_TestCase2 {

	private $worker_event;

	private $base_test;


	public function __construct() {
		parent::__construct();
		$this->worker_event = new EER_Worker_Event();
		$this->base_test    = new EER_Base_Test();
	}


	public function setUp() {
		$this->base_test->delete_all_data();
		$this->base_test->setUp();
	}


	public function test_create_event() {
		$this->assertEquals(0, count(EER()->event->load_events()));

		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$events = EER()->event->load_events();
		$event1 = $this->base_test->eer_get_event_by_title('Test Event');
		$this->assertEquals(1, count($events));
		$this->assertEquals([$event1->id => $event1], $events);

		do_action('eer_process_event', [
			'title'          => 'Test Event 2',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$events = EER()->event->load_events();
		$event2 = $this->base_test->eer_get_event_by_title('Test Event 2');
		$this->assertEquals(2, count($events));
		$this->assertEquals([$event1->id => $event1, $event2->id => $event2], $events);

		do_action('eer_process_event', [
			'title'          => 'Test Event 3',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);

		$events = EER()->event->load_events();
		$event3 = $this->base_test->eer_get_event_by_title('Test Event 3');
		$this->assertEquals(3, count($events));
		$this->assertEquals([$event1->id => $event1, $event2->id => $event2, $event3->id => $event3], $events);
	}

}
