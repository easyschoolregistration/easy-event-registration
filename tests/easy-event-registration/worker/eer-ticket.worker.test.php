<?php
include_once EER_PLUGIN_PATH . '/tests/eer-base-test.php';

class EER_Ticket_Worker_Test extends PHPUnit_Framework_TestCase2 {

	private $worker_event;

	private $worker_ticket;

	private $base_test;


	public function __construct() {
		parent::__construct();
		$this->worker_event  = new EER_Worker_Event();
		$this->worker_ticket = new EER_Worker_Ticket();
		$this->base_test     = new EER_Base_Test();
	}


	public function setUp() {
		$this->base_test->delete_all_data();
		$this->base_test->setUp();
	}


	public function test_create_ticket() {
		$this->assertEquals(0, count(EER()->ticket->load_tickets()));

		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->assertEquals(0, count(EER()->ticket_summary->eer_get_ticket_by_event($event->id)));

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$this->assertEquals(1, count(EER()->ticket->load_tickets()));
		$this->assertEquals(1, count(EER()->ticket_summary->eer_get_ticket_by_event($event->id)));

		$this->worker_ticket->process_ticket([
			'title'           => 'Test Ticket 2',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		]);

		$this->assertEquals(2, count(EER()->ticket->load_tickets()));
		$this->assertEquals(2, count(EER()->ticket_summary->eer_get_ticket_by_event($event->id)));
	}


	public function test_update_ticket() {
		$this->assertEquals(0, count(EER()->ticket->load_tickets()));

		do_action('eer_process_event', [
			'title'          => 'Test Event',
			'sale_start'     => date('Y-m-d H:i:s'),
			'sale_end'       => date('Y-m-d H:i:s', strtotime("+1 day")),
			'event_settings' => ''
		]);
		$event = $this->base_test->eer_get_event_by_title('Test Event');

		$this->assertEquals(0, count(EER()->ticket_summary->eer_get_ticket_by_event($event->id)));

		$ticket_data = [
			'title'           => 'Test Ticket',
			'event_id'        => $event->id,
			'price'           => 200,
			'max_per_order'   => 1,
			'position'        => 0,
			'is_solo'         => false,
			'ticket_settings' => ''
		];

		$this->worker_ticket->process_ticket($ticket_data);

		$ticket = $this->base_test->eer_get_ticket_by_title('Test Ticket');

		$this->assertEquals($ticket_data['title'], $ticket->title);
		$this->assertEquals($ticket_data['price'], $ticket->price);
		$this->assertEquals($ticket_data['max_per_order'], $ticket->max_per_order);
		$this->assertEquals($ticket_data['position'], $ticket->position);
		$this->assertEquals($ticket_data['is_solo'], (boolean) $ticket->is_solo);

		$ticket_data = [
			'ticket_id' => $ticket->id,
			'title'           => 'Test Ticket 2',
			'event_id'        => $event->id,
			'price'           => 500,
			'max_per_order'   => 5,
			'position'        => 2,
			'is_solo'         => false,
			'ticket_settings' => ''
		];

		$this->worker_ticket->process_ticket($ticket_data);

		$ticket = EER()->ticket->get_ticket_data($ticket->id);

		$this->assertEquals($ticket_data['title'], $ticket->title);
		$this->assertEquals($ticket_data['price'], $ticket->price);
		$this->assertEquals($ticket_data['max_per_order'], $ticket->max_per_order);
		$this->assertEquals($ticket_data['position'], $ticket->position);
		$this->assertEquals($ticket_data['is_solo'], (boolean) $ticket->is_solo);

		$ticket_data = [
			'ticket_id' => $ticket->id,
			'title'           => 'Test Ticket 2',
			'event_id'        => $event->id,
			'price'           => 500,
			'max_per_order'   => 5,
			'position'        => 2,
			'max_leaders' => 20,
			'max_followers' => 30,
			'is_solo'         => false,
			'ticket_settings' => ''
		];

		$ticket_summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals($ticket_summary->max_leaders, 0);
		$this->assertEquals($ticket_summary->max_followers, 0);
		$this->assertEquals($ticket_summary->max_tickets, 0);

		$this->worker_ticket->process_ticket($ticket_data);

		$ticket_summary = EER()->ticket_summary->eer_get_ticket_summary($ticket->id);

		$this->assertEquals($ticket_summary->max_leaders, 20);
		$this->assertEquals($ticket_summary->max_followers, 30);
		$this->assertEquals($ticket_summary->max_tickets, 0);
	}

}
