<?php
/**
 * Class SampleTest
 *
 * @package ESR
 */

/**
 * Sample test case.
 */
class EER_Base_Test {

	public $worker_ajax;

	public function __construct() {
		$this->worker_ajax = new EER_Worker_Ajax();
	}


	public function setUp() {
		global $wpdb;

		$u = wp_get_current_user();
		$u->add_role('administrator');
	}


	public function delete_all_data() {
		global $wpdb;
		global $esr_settings;

		$esr_settings = [];
		delete_option('esr_settings');

		$wpdb->query("DELETE FROM {$wpdb->prefix}eer_ticket_summary");
		$wpdb->query("DELETE FROM {$wpdb->prefix}eer_sold_tickets");
		$wpdb->query("DELETE FROM {$wpdb->prefix}eer_events_payments");
		$wpdb->query("DELETE FROM {$wpdb->prefix}eer_tickets");
		$wpdb->query("DELETE FROM {$wpdb->prefix}eer_events");

		$wpdb->query("DELETE FROM {$wpdb->usermeta}");
		$wpdb->query("DELETE FROM {$wpdb->users}");

		wp_cache_flush();
	}

	public function eer_get_event_by_title($title) {
		global $wpdb;
		$event = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}eer_events WHERE title LIKE %s", [$title]));
		$settings = $event->event_settings;
		unset($event->event_settings);
		return (object) array_merge((array) $event, (array) json_decode($settings, true));
	}

	public function eer_get_ticket_by_title($title) {
		global $wpdb;
		$ticket = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}eer_tickets WHERE title LIKE %s", [$title]));
		$settings = $ticket->ticket_settings;
		unset($ticket->ticket_settings);
		return (object) array_merge((array) $ticket, (array) json_decode($settings, true));
	}


	public function eer_get_user_id_by_email($email) {
		return get_user_by('email', $email)->ID;
	}

}
